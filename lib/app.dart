import 'package:flutter/material.dart';
import 'package:indo_news_bloc/utils/routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: appRoutes,
      title: 'Indo News',
    );
  }
}
