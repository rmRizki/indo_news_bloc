import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:indo_news_bloc/data/models/models.dart';
import 'package:indo_news_bloc/data/repositories/repositories.dart';

part 'articles_event.dart';
part 'articles_state.dart';

class ArticlesBloc extends Bloc<ArticlesEvent, ArticlesState>{
  final ArticlesRepository articlesRepository;

  ArticlesBloc({@required this.articlesRepository})
      : assert(articlesRepository != null),
        super(ArticlesInitial());

  @override
  Stream<ArticlesState> mapEventToState(ArticlesEvent event) async* {
    if (event is ArticlesRequested) {
      yield* _mapArticlesRequestedToState(event);
    }
  }

  Stream<ArticlesState> _mapArticlesRequestedToState(
    ArticlesRequested event,
  ) async* {
    yield ArticlesLoadInProgress();
    try {
      final Articles articles = await articlesRepository.fetchTopHeadlines();
      yield ArticlesLoadSuccess(articles: articles.articles);
    } catch (err) {
      print(err);
      yield ArticlesLoadFailure(err: err);
    }
  }
}
