import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:indo_news_bloc/app.dart';
import 'package:indo_news_bloc/blocs/blocs.dart';
import 'package:indo_news_bloc/data/providers/providers.dart';
import 'package:indo_news_bloc/data/repositories/repositories.dart';
import 'package:indo_news_bloc/helpers/network_helper.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  final articlesRepository = ArticlesRepository(
    articlesProvider: ArticlesProvider(
      networkHelper: NetworkHelper(),
    ),
  );

  runApp(
    BlocProvider<ArticlesBloc>(
      create: (context) => ArticlesBloc(
        articlesRepository: articlesRepository,
      ),
      child: App(),
    ),
  );
}
